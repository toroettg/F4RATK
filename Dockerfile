FROM python:3.10.1-slim-bullseye AS backend-build

WORKDIR /usr/src/app/

RUN set -xe pipefail  \
  && pip install poetry

COPY pyproject.toml poetry.lock README.md LICENSE COPYING ./

RUN set -xe pipefail  \
  && poetry export -E server -E server-engine -o requirements.txt

COPY f4ratk/ ./f4ratk/

RUN set -xe pipefail  \
  && poetry build -f wheel

FROM python:3.10.1-slim-bullseye

WORKDIR /usr/src/app/

COPY --from=backend-build /usr/src/app/requirements.txt ./requirements.txt

RUN set -xe pipefail \
  && pip install -r requirements.txt

COPY --from=backend-build /usr/src/app/dist/*.whl ./

RUN set -xe pipefail \
  && pip install f4ratk-*.whl \
  && rm -v /usr/src/app/*

CMD ["gunicorn", "f4ratk.web.server:create_app()"]
