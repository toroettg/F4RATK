from pathlib import Path
from textwrap import dedent

from pytest import raises

from f4ratk.domain import Currency, Frame, Frequency, Region
from f4ratk.file.reader import FileConfig, ValueFormat
from f4ratk.portfolio.ports import PortfolioRequest
from f4ratk.portfolio.reader.api import PortfolioReaderAdapter
from f4ratk.ticker.reader import Stock


def given_a_single_portfolio_file_when_reading_should_return_config_and_sources(
    tmp_path: Path,
):
    testfile = tmp_path.joinpath('portfolio.yml')

    def given():
        with open(testfile, 'w') as handle:
            handle.write(
                dedent(
                    """
                    name: first

                    tickers:
                      - description: USA SmallCap Value
                        symbol: ussc.l
                        currency: usd
                        weight: 90

                    files:
                      - description: EU SmallCap Value
                        path: /nonexistent/path
                        currency: eur
                        format: price
                        weight: 10

                    analysis:
                      region: developed-ex-us
                      frequency: monthly
                    """
                )
            )

    given()

    result = PortfolioReaderAdapter().read(PortfolioRequest(path=testfile, name=None))

    assert result.config.region == Region.DEVELOPED_EX_US
    assert result.config.frame == Frame(
        frequency=Frequency.MONTHLY, start=None, end=None
    )
    assert result.total_weight() == 100

    assert result.sources[0].origin == Stock(
        ticker_symbol='USSC.L', currency=Currency.USD
    )
    assert result.sources[0].weight == 90

    assert result.sources[1].origin == FileConfig(
        path=Path('/nonexistent/path'),
        currency=Currency.EUR,
        value_format=ValueFormat.PRICE,
    )
    assert result.sources[1].weight == 10


def given_a_multi_portfolio_file_when_reading_specific_name_should_return_matching_config_and_sources(  # noqa: E501
    tmp_path: Path,
):
    testfile = tmp_path.joinpath('portfolio.yml')

    def given():
        with open(testfile, 'w') as handle:
            handle.write(
                dedent(
                    """
                    ---
                    name: first

                    analysis:
                      region: us
                      frequency: monthly
                    ...
                    ---
                    name: second

                    analysis:
                      region: EU
                      frequency: DAILY
                    ...
                    """
                )
            )

    given()

    result = PortfolioReaderAdapter().read(
        PortfolioRequest(path=testfile, name='second')
    )

    assert result.config.region == Region.EU
    assert result.config.frame.frequency == Frequency.DAILY
    assert result.config.frame.start is None
    assert result.config.frame.end is None
    assert result.total_weight() == 0


def given_a_multi_portfolio_file_when_reading_unmatched_name_should_exit(
    tmp_path: Path,
):
    testfile = tmp_path.joinpath('portfolio.yml')

    def given():
        with open(testfile, 'w') as handle:
            handle.write(
                dedent(
                    """
                    ---
                    name: first

                    analysis:
                      region: us
                      frequency: monthly
                    ...
                    ---
                    name: second

                    analysis:
                      region: EU
                      frequency: DAILY
                    ...
                    """
                )
            )

    given()

    with raises(SystemExit) as error:
        PortfolioReaderAdapter().read(PortfolioRequest(path=testfile, name='invalid'))

    assert error.value.code == "Unable to read requested portfolio configuration."
