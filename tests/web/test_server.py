##############################################################################
# Copyright (C) 2020 - 2023 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of f4ratk.
#
# f4ratk is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from flask import Flask
from pytest import fixture


@fixture(scope='function')
def mail_server_config(create_env) -> None:
    create_env(
        {
            'MAIL_FROM_ADDRESS': "from@example.org",
            'MAIL_TO_ADDRESS': "to@example.org",
            'MAIL_SERVER_USERNAME': "server-login",
            'MAIL_SERVER_PASSWORD': "1234",
            'MAIL_SERVER_HOST': "mail.example.org",
        }
    )


def given_mail_server_config_when_running_server_should_activate_mail_feature(
    mail_server_config, app: Flask
):
    assert '/v0/mails' in [str(route) for route in app.url_map.iter_rules()]


def given_missing_mail_server_config_when_running_server_should_deactivate_mail_feature(
    app: Flask,
):
    assert '/v0/mails' not in [str(route) for route in app.url_map.iter_rules()]
