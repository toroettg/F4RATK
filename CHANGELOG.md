# Changelog

## [Unreleased]

## [2023.1] - 2023-11-19

### Added

- Support Python 3.11

### Fixed

- Restore the ticker function